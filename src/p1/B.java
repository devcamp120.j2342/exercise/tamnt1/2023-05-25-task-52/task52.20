﻿package p1;

public class B {

    public void accessVariables() {
        A obj = new A();
        // The field A.i is not visible
        obj.i = 20;
        obj.j = 20;

        obj.k = 30;

        obj.l = 40;

        System.out.println(obj);
    }

}
