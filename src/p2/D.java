﻿import p1.A;

public class D {
    public void accessVariables() {
        A obj = new A();
        // The field A.i is not visible
        obj.i = 40;
        obj.j = 20;
        // The field A.k is not visible
        obj.k = 30;
        obj.l = 40;

        System.out.println(obj);
    }
}
