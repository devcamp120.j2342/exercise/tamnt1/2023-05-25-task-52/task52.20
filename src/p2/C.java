﻿
import p1.A;

public class C extends A {

    public void accessVariables() {
        // The field A.i is not visible
        super.i = 20;
        super.j = 20;

        super.k = 30;

        super.l = 40;

        System.out.println();
    }
}
